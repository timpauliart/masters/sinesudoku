from moviepy.video.fx.all import speedx
from operator import attrgetter
from itertools import cycle


def score9(videosizer, videopool, videoon, solver):
    print("SECTION 9 VIDEO")

    assert solver.finished()
    su = solver.sudoku
    recs = 3
    row = solver.sudoku.rows[8]
    box = solver.sudoku.boxes[8]
    column = solver.sudoku.columns[8]

    rhythms = [row, box, column]
    rhythms = map(lambda y: list(map(lambda x: int(x), y)), rhythms)
    rhythms = cycle(rhythms)

    dur = 45
    dur2 = 2

    videos = []
    for name in list(videopool["faces"].keys()):
            videos.append(videopool["faces"].pop(name))

    assert(len(videopool["faces"]) == 0)

    last = max(videos, key=attrgetter('duration'))
    videos.remove(last)

    real_total = sum(map(lambda x: x.duration, videos))

    for v, rhy in zip(videos, rhythms):
        duration = (v.duration / real_total) * dur
        vi = speedx(v, final_duration=duration)
        if videoon:
            videosizer.rasterize_sudoku(vi, su, 2, 2, 1, rhy, recs)

    last = speedx(v, final_duration=dur2*2)
    if videoon:
        videosizer.rasterize_sudoku(last, su, 2, 2, 1, [1] * 9, recs)
