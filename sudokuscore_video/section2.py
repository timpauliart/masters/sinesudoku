from itertools import cycle, islice, permutations


def score2(videosizer, videopool, videoon, solver):
    print("SECTION 2 VIDEO")

    row = solver.sudoku.rows[1]
    box = solver.sudoku.boxes[1]
    column = solver.sudoku.columns[1]
    recs = 1
    rhythm = 1 / 8
    hits = 3 * 6 * 9

    def createPermutations():
        base = []
        base.append(row)
        base.append(box)
        base.append(column)
        perms = list(permutations(base))
        return perms

    def tri(clip, fund, perm, index, dur, rhy_micro):
        if clip.duration < dur:
            raise ValueError("clip.duration < dur")
        start = (clip.duration - dur) * (index / 9)
        clip = clip.subclip(start, start + dur)
        su = solver.next()
        zo = 1 + 3 * perm[1][index].normalize()
        sp = fund + 1 + 3 * perm[0][index].normalize()  # fund goes to 0
        nf = 3 + 9 * perm[2][index].normalize()
        if videoon:
            videosizer.rasterize_sudoku(clip, su, zo, sp, nf, rhy_micro, recs)

    funds = createFundamentals(hits)
    perms = cycle(createPermutations())
    indices = cycle(range(9))

    blueprints = [([1] * i) + ([0] * (9 - i)) for i in range(1, 4)]
    rhy_micros = [list(islice(cycle(set(permutations(rhy))), 0, 3 * 18))
                  for rhy in blueprints]
    rhy_micros = list(
        reversed([val for sublist in rhy_micros for val in sublist]))
    assert(len(rhy_micros) == 162)

    videos = []
    names = ["mikrophonie_drum9",  # drum intro
             "jeita_stuff1",
             "mikrophonie_switch4",
             "momente_god1",  # fixed because final (permutations)
             "jeita_mixer6",
             "jeita_radio2"]
    for name in names:
        videos.append(videopool["hands"].pop(name))

    videos = permutations(videos)
    videos = islice(videos, 0, None, 9)
    videos = [val for sublist in videos for val in sublist]

    durs = ([rhythm] * ((5 * 18) - 1)) + [0.075] + ([rhythm] * (4 * 18))
    assert(sum(durs) == 20.2)

    cascade = zip(videos, funds, perms, indices, rhy_micros, durs)
    for clip, fund, perm, index, rhy_micro, dur in cascade:
        tri(clip, fund, perm, index, dur, rhy_micro)


def createFundamentals(hits):
    assert hits % 27 == 0
    repeat = hits // 27
    for note in reversed(range(27)):
        for i in range(repeat):
            yield note
