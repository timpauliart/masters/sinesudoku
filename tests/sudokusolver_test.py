import unittest

from sinesudoku.sudoku import Sudoku
from sinesudoku.sudokusolver import Sudokusolver
from sudoku_test import testempty2d, testfull2d


class SudokusolverTests(unittest.TestCase):

    def setUp(self):
        self.size = 9
        self.empty = Sudoku(testempty2d(), self.size)
        self.full = Sudoku(testfull2d(), self.size)
        self.solver = Sudokusolver(self.empty, 0)

    def testSolve(self):
        self.solver.solve()
        result = self.solver.sudoku
        for r1, r2 in zip(result.rows, self.full.rows):
            self.failUnless(list(r1) == list(r2))


if __name__ == '__main__':

    unittest.main()
