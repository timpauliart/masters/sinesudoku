import os
import unittest

from sinesudoku import sudokusynth


class SudokusynthTests(unittest.TestCase):

    def setUp(self):
        self.synth = sudokusynth.Sudokusynth("test")

    def testSingleSine(self):
        name = self.synth.build_dir + self.synth.filename + self.synth.score_end
        try:
            self.synth.synthesize_sine(sudokusynth.Synthtype.LINE,
                                       0, 1, 0, 0.4, 0.4, 1, 1,
                                       [126, 135, 90, 153, 117, 144, 162, 99, 108],
                                       [5, 3, 2, 9, 7, 4, 8, 1, 6],
                                       [5, 3, 4, 6, 7, 8, 9, 1, 2],
                                       [5, 2, 1, 9, 8, 7, 6, 4, 3])

            correct_answere = ["i", "\"sudokulinesine\"",
                               "0.000000", "1.000000", "0.000000", "0.400000", "0.400000", "1.000000", "1.000000",
                               "/**/",
                               "126", "135", "90", "153", "117", "144", "162", "99", "108",
                               "/**/",
                               "5", "3", "2", "9", "7", "4", "8", "1", "6",
                               "/**/",
                               "5", "3", "4", "6", "7", "8", "9", "1", "2",
                               "/**/",
                               "5", "2", "1", "9", "8", "7", "6", "4", "3", "1.000000"]

            with open(name, 'r') as f:
                content = f.readline()
                content = content.strip()
                content = content.split()
                self.failUnless(content == correct_answere)
        finally:
            os.remove(name)

    def testRepeatsToHz1(self):
        self.failUnless(self.synth.repeats_to_hz(1, 2) == 0.5)

    def testRepeatsToHz2(self):
        self.failUnless(self.synth.repeats_to_hz(25, 5) == 5)

    def testKillZeros(self):
        result = [0, 1, 2, 3, 0, 7, 8, 9, 0]
        result = list(self.synth.kill_zeros(result))
        dur = self.synth.shortest_dur
        correct_answere = [dur, 1, 2, 3, dur, 7, 8, 9, dur]
        self.failUnless(result == correct_answere)

    def testComputeTimeEnvelope(self):
        env = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        self.failUnless(self.synth.compute_time_envelope(env) == env)

    def testComputeAmpEnvelope1(self):
        env = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        res = self.synth.compute_amp_envelope(env, 9)
        self.failUnless(res == env)

    def testComputeAmpEnvelope2(self):
        env = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        res = self.synth.compute_amp_envelope(env, 0)
        self.failUnless(res == [0 for i in range(len(env))])

    def testComputeFreqEnvelope1(self):
        env = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        res = self.synth.compute_freq_envelope(env, 81, 9)
        self.failUnless(res == [81 + i for i in env])

    def testComputeFreqEnvelope2(self):
        env = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        res = self.synth.compute_freq_envelope(env, 440, 0)
        self.failUnless(res == [440 for i in range(len(env))])


if __name__ == '__main__':

    unittest.main()
