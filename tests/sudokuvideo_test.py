import unittest

from sinesudoku.sudokuvideo import Sudokuvideosizer


class SudokusynthTests(unittest.TestCase):

    def setUp(self):
        self.v = Sudokuvideosizer()

    def testKillUniqueMax(self):
        result = self.v.kill([1, 1], lambda x: x <= 1)
        self.failUnless(len(result) == 2)
        self.failUnless(result[0] == 2)
        self.failUnless(result[1] is None)


if __name__ == '__main__':

    unittest.main()
