from sinesudoku.sudokuinterface import ratios, scale
from sinesudoku.sudokusynth import Synthtype


def score7(synth, time, max_amp, soundon, solver):
    """
         INTERLUDE I - POINT-LINE-POINT
    """
    print("SECTION 7")
    currenttime = time

    s = solver.next()
    dur = 0.5
    pitch = scale[0]
    glis = ratios[9]
    amp = max_amp
    if soundon:
        assert amp <= max_amp
        synth.synthesize_sudoku(currenttime, s,
                                Synthtype.POINT,
                                dur, 0, dur,
                                pitch, glis, amp, 3200, 3200)
    currenttime += dur

    return currenttime
