from sinesudoku.sudokuinterface import ratios, scale
from sinesudoku.sudokusynth import Synthtype


def score9(synth, time, max_amp, soundon, solver):
    """
        FINAL - SENTENCE
        the sudoku is solved.
        we get a kandinsky-like sentence.
        one godly major-chord is fading to noise.
    """
    print("SECTION 9")
    currenttime = time

    assert solver.finished()
    s = solver.sudoku

    dur = 45
    dur2 = 2
    fade = 1 / 81

    for note in [9, 18 + 3, 27 + 5]:
        if soundon:
            synth.synthesize_sudoku(currenttime, s,
                                    Synthtype.LINE,
                                    dur, fade, fade,
                                    scale[note], ratios[9], max_amp)

    for note, att in zip([9 + 3, 9 + 5, 18, 18 + 5, 27, 27 + 3],
                         [6 / 9, 5 / 9, 4 / 9, 3 / 9, 2 / 9, 1 / 9]):
        a = dur * att
        t = currenttime + dur - a
        if soundon:
            synth.synthesize_sudoku(t, s,
                                    Synthtype.LINE,
                                    a, a - fade, fade,
                                    scale[note], ratios[9], max_amp)

    currenttime += dur + dur2

    for note in [9, 18 + 3, 27 + 5]:
        if soundon:
            synth.synthesize_sudoku(currenttime, s,
                                    Synthtype.POINT,
                                    dur2, 0, dur2,
                                    scale[note], ratios[9], max_amp + 6)
    currenttime += dur2

    return currenttime
