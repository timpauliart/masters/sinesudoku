from sinesudoku.sudokuinterface import ratios, scale
from sinesudoku.sudokusynth import Synthtype


def score4(synth, time, max_amp, soundon, solver, cue):
    """
         "Lock S-foils in attack position." - POINTS
         uses the numbers of the x-wing-pilots.
         one after another joins in and stays.
         glissandos for opening the s-foils.
    """
    print("SECTION 4")
    currenttime = time

    pilots = [10, 7, 3, 6, 9, 2, 11, 5]
    length = sum(pilots)
    done = 0

    for red in pilots:
        s = solver.next()
        dur = length - done
        pitch = scale[red]
        up = scale[red + red]
        down = scale[0]
        glis = ratios[red % 9]
        amp = max_amp - (red / 11)
        if currenttime < cue:
            att = cue - currenttime
        else:
            att = 0
        if soundon:
            assert amp <= max_amp
            synth.synthesize_sudoku(currenttime, s,
                                    Synthtype.POINT,
                                    dur, att, 0,
                                    pitch, glis, amp, glisaim=up)
            synth.synthesize_sudoku(currenttime, s,
                                    Synthtype.POINT,
                                    dur, att, 0,
                                    pitch, glis, amp, glisaim=down)
        done += red
        currenttime += red

    return currenttime
