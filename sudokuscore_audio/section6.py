import itertools

from sinesudoku.sudokuinterface import ratios, scale
from sinesudoku.sudokusynth import Synthtype


def score6(synth, time, max_amp, soundon, solver, duration0):
    """
         "Yahoo!" - POINT-LINES
         uses the music of john williams (force-theme-rhythm).
         glitchy.
         #nocontrol.
    """
    print("SECTION 6")
    currenttime = time
    row = solver.sudoku.rows[5]
    box = solver.sudoku.boxes[5]
    column = solver.sudoku.columns[5]

    lens = [1,
            2, 1.5, 0.25, 0.25,
            2, 1.5, 0.5,
            1.5, 0.5, 0.5, 0.5, 1 / 3, 1 / 3, 1 / 3,
            3]

    atts = itertools.cycle(map(lambda x: x.normalize(), row))
    notes = itertools.cycle(map(lambda x: int(x), box))
    amps = itertools.cycle(
        map(lambda x: max_amp - x.normalize(), column))

    s = solver.next()
    if soundon:
        synth.synthesize_sudoku(currenttime - duration0, s,
                                Synthtype.POINT,
                                duration0, duration0 - 1 / 81, 1 / 81,
                                scale[int(box[0])], ratios[int(box[0])],
                                max_amp - 6, 1, 3200 * duration0,
                                -1, 3)
    currenttime += 1

    # drop
    for i, l, att, note, amp in zip(itertools.count(0, 1), lens, atts, notes, amps):
        s = solver.next()
        if i == 0:
            amp = max_amp
        if soundon:
            assert amp <= max_amp
            synth.synthesize_sudoku(currenttime, s,
                                    Synthtype.POINT,
                                    l, l * att - 1 / 81, 1 / 81,
                                    scale[note], ratios[note], amp, 1, 3200,
                                    -1, 3)
        currenttime += l

    return currenttime
