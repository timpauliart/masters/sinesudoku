from sinesudoku.sudokuinterface import scale
from sinesudoku.sudokusynth import Synthtype


def score3(synth, time, max_amp, soundon, solver):
    """
        DANCING LINKS - LINE-POINTS
        inspired by the implementation of donald knuths algorithm x.
        glissandi = links. (need for negative glis)
        abstract-techno-party in slow-motion.
    """
    print("SECTION 3")
    currenttime = time
    row = solver.sudoku.rows[2]
    box = solver.sudoku.boxes[2]
    column = solver.sudoku.columns[2]

    sec3begin = time

    class DancingChords:

        def __init__(self, pitches, amps, durs):
            """pitches in ascending order?"""
            assert(pitches == sorted(pitches))
            length = len(pitches)
            data = zip(pitches, amps, durs)
            self.nodes = [ChordNode(p, a, d, None, None) for p, a, d in data]
            for i in range(length - 1):
                self.nodes[i].above = self.nodes[i + 1]
            for i in reversed(range(1, length)):
                self.nodes[i].below = self.nodes[i - 1]
            self.nodes[0].below = self.nodes[0]
            self.nodes[-1].above = self.nodes[-1]

    class ChordNode:

        def __init__(self, pitch, amp, dur, below, above):
            self.pitch = pitch
            self.amp = amp
            self.dur = dur
            self.below = below
            self.above = above

        def synthesize(self, time, stretch):
            s = solver.next()
            assert(self.pitch >= self.below.pitch)
            down = -(self.below.pitch / self.pitch)
            if down == -1:
                down = 0
            dur = self. dur * stretch
            if soundon:
                assert(self.amp <= max_amp)
                synth.synthesize_sudoku(time, s, Synthtype.LINE,
                                        dur, 0, dur,
                                        self.pitch, down, self.amp)

        def __repr__(self):
            rep = str(self.pitch)
            return rep

        def delete(self):
            self.below.above = self.above
            self.above.below = self.below
            return self

    timestep = 4
    # kick
    pitches = [scale[i] for i in range(9)]
    amps = list(map(lambda x: max_amp - x.normalize(), box))
    durs = list(map(lambda x: x.normalize() * (timestep / 2), row))

    kick = DancingChords(pitches, amps, durs)
    stretches = [1 / 729, 1 / 729,  1 / 81, 1 / 81, 1 / 9, 1 / 9, 1, 3, 8]
    for destroy, stretch in zip(column, stretches):
        for node in kick.nodes:
            node.synthesize(currenttime, stretch)
        kick.nodes[int(destroy) - 1].delete()
        currenttime += timestep

    sec4cue = currenttime - timestep
    return sec3begin, sec4cue
