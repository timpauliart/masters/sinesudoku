from itertools import chain, cycle, permutations

from sinesudoku.sudokuinterface import ratios, scale
from sinesudoku.sudokusynth import Synthtype


def score2(synth, time, max_amp, soundon, solver):
    """
        BRUTE FORCE - LINE TO LINE-POINTS
        inspired by brute force algorithms.
        trying all combinations of mappings.
        stupid cliche-sound-cascade NOT!
    """
    print("SECTION 2")
    currenttime = time
    row = solver.sudoku.rows[1]
    box = solver.sudoku.boxes[1]
    column = solver.sudoku.columns[1]
    rhythm = 1 / 8
    hits = 3 * 6 * 9

    def createPermutations():
        base = []
        base.append(row)
        base.append(box)
        base.append(column)
        perms = list(permutations(base))
        return perms

    def tri(currenttime, perm, index, dur, att_rat, amp_low, rhy, last_n):
        s = solver.next()
        note = 9 + index + int(perm[0][index])
        if note == last_n:
            note += 1
        pitch = scale[note]
        glis = ratios[int(perm[1][index])]
        amp = amp_low + perm[2][index].normalize() * abs(max_amp - amp_low)
        att = att_rat * dur
        rel = dur - att
        if soundon:
            assert amp <= max_amp
            synth.synthesize_sudoku(currenttime, s,
                                    Synthtype.LINE,
                                    dur, att, rel,
                                    pitch, glis, amp)

        return currenttime + rhy, note

    length = hits * rhythm
    perms = cycle(createPermutations())
    indices = cycle(range(9))

    durs1 = shorten(2 * 18, length, 9)
    durs2 = shorten(2 * 18, 9, 1)
    durs3 = shorten(4 * 18 + 9, 1, 1 / 9)
    durs4 = shorten(9, 1, 1 / 81)
    durs = list(chain(durs1, durs2, durs3, durs4))
    assert(len(durs) == hits)

    att_rats1 = shorten(2 * 18, 3, 1)
    att_rats2 = shorten(2 * 18, 1, 1 / 9)
    att_rats3 = shorten(2 * 18, 1 / 9, 1 / 81)
    att_rats4 = shorten(3 * 18, 1 / 81, 1 / 729)
    att_rats = list(chain(att_rats1, att_rats2, att_rats3, att_rats4))
    assert(len(att_rats) == hits)

    amp_lows = pushing(hits, max_amp)

    rhys = ([rhythm] * ((5 * 18) - 1)) + [0.075] + ([rhythm] * (4 * 18))
    assert(sum(rhys) == 20.2)

    cascade = zip(perms, indices, durs, att_rats, amp_lows, rhys)
    last_n = None
    for perm, index, dur, att_rat, amp_low, rhy in cascade:
        currenttime, last_p = tri(
            currenttime, perm, index, dur, att_rat, amp_low, rhy, last_n)
    return currenttime


def shorten(hits, start, end):
    """downward spiral of durations"""
    step = computeStep(hits, start, end)
    for i in range(hits):
        dur = start * (step**i)
        assert dur >= end
        yield dur


def pushing(hits, end):
    start = end - 6
    step = computeStep(hits, start, end)
    for i in range(hits):
        yield start * step**i


def computeStep(hits, start, end):
    step = (end / start)**(1 / hits)
    return step
