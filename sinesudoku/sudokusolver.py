class Sudokusolver:

    def __init__(self, sudoku, tries):
        self.sudoku = sudoku
        print(self.sudoku)
        self._cells = []
        for box in self.sudoku.boxes:
            for cell in box:
                if not cell.starter:
                    self._cells.append(cell)
        self._index = 0
        self.counter = 0
        self.tries = tries

    def next(self):
        current = self._cells[self._index]
        add = 1
        while True:
            if current.value + add == 10:
                current.value = 0
                self._index -= 1
                return self.next()
            else:
                try:
                    current.value = current.value + add
                    self.counter += 1
                    print(self.counter)
                    self._index += 1
                    print(self.sudoku, end='')
                    return self.sudoku
                except ValueError:
                    add += 1

    def finished(self):
        return self._index == len(self._cells)

    def solve(self):
        while not self.finished():
            self.next()
        return self.counter

    def togo(self):
        return self.tries - self.counter
