import itertools
import math


class Cell:

    def __init__(self, size, value, box, row, column,
                 boxIndex, rowIndex, columnIndex, starter=False):
        self._value = 0
        self._size = size
        self._box = box
        self.boxIndex = boxIndex
        self._row = row
        self.rowIndex = rowIndex
        self._column = column
        self.columnIndex = columnIndex
        self.value = value
        self.starter = starter

    def check(self, val):
        if val < 0 or self._size < val:
            raise ValueError("out of range")

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self.check(value)
        old = self._value
        try:
            self._value = value
            self._box.check()
            self._row.check()
            self._column.check()
        except ValueError as e:
            self._value = old
            raise e

    def __int__(self):
        return self.value

    @property
    def boxIndex(self):
        return self._boxIndex

    @boxIndex.setter
    def boxIndex(self, index):
        self.check(index)
        self._boxIndex = index

    @property
    def rowIndex(self):
        return self._rowIndex

    @rowIndex.setter
    def rowIndex(self, index):
        self.check(index)
        self._rowIndex = index

    @property
    def columnIndex(self):
        return self._columnIndex

    @columnIndex.setter
    def columnIndex(self, index):
        self.check(index)
        self._columnIndex = index

    def __repr__(self):
        return self.value.__repr__()

    def getBoxPerspective(self):
        return self.getPerspective(self._box)

    def getBoxPerspectiveInverse(self):
        return self.getPerspective(reversed(self._box))

    def getRowPerspective(self):
        return self.getPerspective(self._row)

    def getRowPerspectiveInverse(self):
        return self.getPerspective(reversed(self._row))

    def getColumnPerspective(self):
        return self.getPerspective(self._column)

    def getColumnPerspectiveInverse(self):
        return self.getPerspective(reversed(self._column))

    def getPerspective(self, iterable):
        collect = False
        counter = 0
        for cell in itertools.cycle(iterable):
            if counter == self._size:
                break
            if cell == self:
                collect = True
            if collect:
                yield cell
                counter += 1

    def normalize(self):
        return self.value / self._size

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.value == other.value
        else:
            return self.value == other


class Field:
    """box, column or row"""

    def __init__(self, size, index):
        self._cells = []
        self._size = size
        self._cells = [Cell(size, 0, self, self, self, 0, 0, 0)
                       for i in range(size)]
        self.index = index

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, ndx):
        if ndx < 0 or self._size < ndx:
            raise ValueError("index out of range")
        self._index = ndx

    def __repr__(self):
        return self._cells.__repr__()

    def __iter__(self):
        return self._cells.__iter__()

    def __reversed__(self):
        return self._cells.__reversed__()

    def __getitem__(self, index):
        return self.get(index)

    def get(self, index):
        return self._cells[index]

    def set(self, index, cell):
        self._cells[index] = cell

    def check(self):
        nozeros = [x.value for x in self._cells if x.value != 0]
        s = set(nozeros)
        if not (len(nozeros) == len(s)):
            raise ValueError("sudoku constraints violated")

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._cells == other._cells
        else:
            return self._cells == other


class Sudoku:

    def __init__(self, twodlist, size):
        self._size = size
        self.boxes = [Field(self._size, i) for i in range(size)]
        self.rows = [Field(self._size, i) for i in range(size)]
        self.columns = [Field(self._size, i) for i in range(size)]
        for x in range(size):
            for y in range(size):
                value = twodlist[y][x]  # turn it arround
                boxIndex = self.getBoxIndex(x, y)
                box = self.boxes[boxIndex]
                row = self.rows[y]
                column = self.columns[x]
                indexInBox = self.getIndexInBox(x, y)
                starter = (value != 0)
                cell = Cell(size, value, box, row, column,
                            indexInBox, y, x, starter)
                self.boxes[boxIndex].set(indexInBox, cell)
                self.rows[y].set(x, cell)
                self.columns[x].set(y, cell)

        # check
        self.checkFields(self.boxes)
        self.checkFields(self.rows)
        self.checkFields(self.columns)

    def getBoxIndex(self, x, y):
        boxSize = int(math.sqrt(self._size))
        assert boxSize == math.sqrt(self._size)
        xBox = int(math.floor(x / boxSize))
        yBox = int(math.floor(y / boxSize))
        return yBox * boxSize + xBox

    def getIndexInBox(self, x, y):
        boxSize = int(math.sqrt(self._size))
        assert boxSize == math.sqrt(self._size)
        xBox = x % boxSize
        yBox = y % boxSize
        return yBox * boxSize + xBox

    def getXandYfromBox(self, glob, loc):
        boxSize = int(math.sqrt(self._size))
        assert boxSize == math.sqrt(self._size)
        xLocal = loc % boxSize
        yLocal = (loc - xLocal) / boxSize
        xGlobal = (glob % boxSize)
        yGlobal = (glob - xGlobal)
        xGlobal *= boxSize
        return int(xGlobal + xLocal), int(yGlobal + yLocal)

    @staticmethod
    def checkFields(field):
        for f in field:
            f.check()

    def __repr__(self):
        rep = ""
        for row in self.rows:
            rep += str(row)
            rep += "\n"
        return rep
