from enum import Enum
from errno import EEXIST
from os import makedirs
from subprocess import call

from . import scorewriter


class Synthtype(Enum):
    POINT = "\"sudokupointsine\""
    LINE = "\"sudokulinesine\""


class Sudokusynth:
    build_dir = "build_audio/"
    score_end = ".sco"
    shortest_dur = (1 / (9 * 9))

    def __init__(self, filename, orc="sinesudoku/sudokusines.orc"):

        try:
            makedirs(self.build_dir)
        except OSError as e:
            if e.errno != EEXIST:
                raise

        self.filename = filename
        self.orc = orc

        with open(self.build_dir + self.filename + self.score_end, 'w'):
            pass

    def synthesize_sudoku(self,
                          time, sudoku, pointorline,
                          duration, attack, release,
                          pitch, max_glissando, amp,
                          glissando_repeats=1, crescendo_repeats=1.9,
                          glisaim=-1, max_otones=81):
        """this is the function for raw writing. this means
        duration in seconds, pitch in hertz, etc"""
        if glisaim < 0:  # hacky
            glisaim = pitch
        real_amp = 729 * 10**(amp / 20)
        current_pitch = pitch
        max_glis = max_glissando * pitch
        macro_glis = glisaim / pitch
        counter = 0
        for box in sudoku.boxes:
            for cell in box:
                freq_value_envelope = self.compute_freq_envelope(
                    list(cell.getBoxPerspective()), current_pitch, max_glis)

                freq_time_envelope = self.compute_time_envelope(
                    list(cell.getRowPerspective()))

                amp_value_envelope = self.compute_amp_envelope(
                    cell.getColumnPerspective(), real_amp)

                amp_time_envelope = freq_time_envelope

                freq_repeats = self.repeats_to_hz(
                    (int(cell) / 9) * glissando_repeats, duration)
                amp_repeats = self.repeats_to_hz(
                    (int(cell) / 9) * crescendo_repeats, duration)

                phase = (int(cell) / 10)

                self.synthesize_sine(pointorline, time, duration,
                                     phase,
                                     attack, release,
                                     freq_repeats,
                                     amp_repeats,
                                     freq_value_envelope, freq_time_envelope,
                                     amp_value_envelope, amp_time_envelope,
                                     macro_glis)

                current_pitch += pitch
                counter += 1
                if counter >= max_otones:
                    return

    def synthesize_sine(self, pointorline, time, duration,
                        phase, attack, release,
                        freq_repitions, amp_repitions,
                        freq_value_envelope, freq_time_envelope,
                        amp_value_envelope, amp_time_envelope, glisaim=1):
        # check input
        if (len(amp_value_envelope) != 9
            or len(amp_time_envelope) != 9
            or len(freq_value_envelope) != 9
                or len(freq_time_envelope) != 9):
            raise ValueError("size of envelope != 9")
        if (round(attack + release, 9) > round(duration, 9)):
            raise ValueError("attack + release >= duration",
                             attack, release, duration, attack + release)
        if phase < 0 or 1 < phase:
            raise ValueError("phase seems fishy", phase)
        if amp_repitions < 0 or freq_repitions < 0:
            raise ValueError("repitions < 0")
        if glisaim < 0:
            raise ValueError("glisaim < 0")
        if any(x * glisaim <= 0 or 22050 < x * glisaim for x in freq_value_envelope):
            raise ValueError("ALIASING with macro_glis: " + str(glisaim) +
                             str(freq_value_envelope))

        # write to file
        with open(self.build_dir + self.filename + self.score_end, 'a') as f:
            scorewriter.write_string(f, "i")
            scorewriter.write_string(f, pointorline.value)
            scorewriter.write_single(f, time)
            scorewriter.write_single(f, duration)
            scorewriter.write_single(f, phase)
            scorewriter.write_single(f, attack)
            scorewriter.write_single(f, release)
            scorewriter.write_single(f, freq_repitions)
            scorewriter.write_single(f, amp_repitions)
            scorewriter.write_envelope(f, freq_value_envelope)
            scorewriter.write_envelope(f, freq_time_envelope)
            scorewriter.write_envelope(f, amp_value_envelope)
            scorewriter.write_envelope(f, amp_time_envelope)
            scorewriter.write_single(f, glisaim)
            scorewriter.write_eol(f)

    def silence(self, time, duration):
        with open(self.build_dir + self.filename + self.score_end, 'a') as f:
            scorewriter.write_string(f, "i")
            scorewriter.write_string(f, "\"silence\"")
            scorewriter.write_single(f, time)
            scorewriter.write_single(f, duration)

    @staticmethod
    def repeats_to_hz(repeats, duration):
        """duration in seconds"""
        if repeats <= 0:
            result = 1 / duration
        else:
            result = repeats / duration
        return result

    def compute_time_envelope(self, envelope):
        no_zeros = self.kill_zeros(envelope)
        return list(no_zeros)

    def kill_zeros(self, envelope):
        for cell in envelope:
            if int(cell) == 0:
                yield self.shortest_dur
            else:
                yield int(cell)

    def compute_amp_envelope(self, envelope, max_amp):
        result = list(map(lambda x: (max_amp / 9) * int(x), envelope))
        if any(x < 0 for x in result):
            raise ValueError("amplitude < 0")
        return result

    def compute_freq_envelope(self, envelope, min_pitch, max_glissando):
        step = max_glissando / 9
        result = list(map(lambda x: min_pitch + step * int(x), envelope))
        if any(x <= 0 or 22050 < x for x in result):
            raise ValueError("ALIASING: overtone freq: " +
                             str(min_pitch) + ": " + str(result))
        return result

    def render(self, sr, ksmps):
        call(["csound",
              "--sample-rate=" + str(sr),
              "--control-rate=" + str(sr / ksmps),
              "--logfile=" + self.build_dir + self.filename + ".log",
              "--format=wav",
              "--output=" + self.build_dir + self.filename + ".wav",
              "--format=24bit",
              "--nodisplays",
              self.orc,
              self.build_dir + self.filename + self.score_end])
