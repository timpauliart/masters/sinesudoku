import itertools
import os
import uuid
from errno import EEXIST
from itertools import accumulate
from os import makedirs

from moviepy.editor import (AudioFileClip, ColorClip, CompositeVideoClip,
                            VideoFileClip)
from moviepy.video.fx.all import crop, loop, speedx

# from moviepy.editor import concatenate_videoclips
# from moviepy.video.fx.all import supersample
# TODO local fix
from .concatenate_debug import concatenate_videoclips
from .supersample_debug import supersample


def mkdir(name):
    try:
        makedirs(name)
    except OSError as e:
        if e.errno != EEXIST:
            raise


class Sudokuvideosizer:
    build_dir = "build_video/"

    def __init__(self):
        mkdir(self.build_dir)
        self.clips = []
        self.tmps = []

    def reset(self):
        self.clips = []
        self.remove_tmps()

    def rasterize_sudoku(self, inputclip, sudoku, zoom_max, speed_max,
                         supersample_max, times, recursions):
        if zoom_max < 1:
            raise ValueError("zoom_max < 1")
        if speed_max < 1:
            raise ValueError("speed_max < 1")
        if supersample_max < 1:
            raise ValueError("supersample_max < 1")
        if len(times) != 9:
            raise ValueError("len(times) != 9")

        outputclip = self.process_box_timed(inputclip, sudoku, zoom_max,
                                            speed_max, supersample_max,
                                            times, recursions)
        self.clips.append(outputclip)

    def process_box_timed(self, inputclip, sudoku, zoom_max, speed_max,
                          supersample_max, times, recursions):
        cls = self
        print(str(recursions) + " recursions left in TIME")
        if recursions == 0:
            return inputclip
        else:
            pieces = cls.cut_into_pieces(inputclip, times)
            pieces_result = []
            for piece, box, i in zip(pieces, sudoku.boxes, range(9)):
                if piece is not None:
                    cells, positions = cls.process_box(
                        piece, box, zoom_max, speed_max, supersample_max)
                    cells_result = []
                    for cell, box in zip(cells, sudoku.boxes):
                        if cell is not None:
                            cell_tmp = cls.process_box_spaced(
                                cell, box, zoom_max, speed_max, supersample_max,
                                recursions - 1)
                            cells_result.append(cell_tmp)
                        else:
                            cells_result.append(None)
                    piece_tmp = cls.composite(cells_result, positions, piece)
                    pieces_result.append(piece_tmp)
            result = cls.concatenate(pieces_result)
            result = cls.render_tmp(result, "TIME/")
            result = self.check_dur(inputclip, result)
            return result

    def process_box_spaced(self, inputclip, box, zoom_max, speed_max,
                           supersample_max, recursions):
        cls = self
        print(str(recursions) + " recursions left in SPACE")
        if recursions == 0:
            return inputclip
        else:
            cells, positions = cls.process_box(
                inputclip, box, zoom_max, speed_max, supersample_max)
            cells = filter(lambda x: x is not None, cells)
            cells = map(lambda x: cls.process_box_spaced(
                x, box, zoom_max, speed_max, supersample_max,
                recursions - 1), cells)
            result = cls.composite(cells, positions, inputclip)
            result = cls.render_tmp(result, "SPACE/")
            result = self.check_dur(inputclip, result)
            return result

    def process_box(self, inputclip, box, zoom_max, speed_max, supersample_max):
        cls = self
        # sizes
        heights = cls.compute_sizes(
            list(itertools.islice(box, 0, None, 3)), inputclip.h)
        width_groups = []
        for i in [0, 3, 6]:
            width_groups.append(cls.compute_sizes(
                list(itertools.islice(box, i, i + 3)), inputclip.w))

        # positions
        height_positions = accumulate(([0] + heights)[:-1])
        width_postion_groups = [accumulate(
            ([0] + i)[:-1]) for i in width_groups]

        # crops
        cells = []
        positions = []
        loop = zip(heights, height_positions,
                   width_groups, width_postion_groups)
        for height, height_position, widths, width_positions in loop:
            for width, width_position in zip(widths, width_positions):
                if width > 0 and height > 0:
                    cell = crop(inputclip, x1=width_position,
                                y1=height_position, width=width, height=height)
                    cells.append(cell)
                else:
                    cells.append(None)
                positions.append((width_position, height_position))

        # processing
        clips = []
        for number, video in zip(box, cells):
            if video is not None:
                clip = cls.process_cell(
                    number, video, zoom_max, speed_max, supersample_max)
                clips.append(clip)
            else:
                clips.append(None)

        # return
        assert(len(clips) == 9)
        assert(len(positions) == 9)
        return clips, positions

    def process_cell(self, cell, video, zoom_max, speed_max, supersample_max):
        cls = self
        result = video
        # zoom
        if zoom_max > 1:
            zoom_unit = (zoom_max - 1) / 9
            zoom_factor = 1 + (int(cell) * zoom_unit)
            result = cls.zoom(video, zoom_factor)

        if speed_max > 1 or supersample_max > 1:
            pieces = cls.cut_into_pieces(result, list(
                map(lambda x: int(x), cell.getRowPerspective())))

            # speedx
            if speed_max > 1:
                speed_unit = (speed_max - 1) / 9
                speeded = []
                for piece, number in zip(pieces, cell.getBoxPerspective()):
                    if piece is not None:
                        dur_original = piece.duration
                        speed_factor = 1 + (int(number) * speed_unit)
                        sp_tmp = speedx(piece, speed_factor)
                        speeded.append(cls.loop_or_cut(sp_tmp, dur_original))
                pieces = speeded
            # supersample
            if supersample_max > 1:
                supersample_unit = (supersample_max - 1) / 9
                supersampled = []
                for piece, number in zip(pieces, cell.getColumnPerspective()):
                    if piece is not None:
                        nframes = 1 + round(int(number) * supersample_unit)
                        su_tmp = supersample(piece, piece.duration / 9, nframes)
                        supersampled.append(su_tmp)
                pieces = supersampled

            # bring together
            result = cls.concatenate(pieces)

        # return
        result = cls.render_tmp(result, "CELL/")
        result = self.check_dur(video, result)
        return result

    @staticmethod
    def loop_or_cut(clip, duration):
        if clip.duration >= duration:
            return clip.subclip(0, duration)
        else:
            return loop(clip, duration=duration)

    @classmethod
    def compute_sizes(cls, cells, wholerange):
        length = len(cells) - 1
        sum_cells = sum(map(lambda x: int(x), cells))
        unit = 1 if sum_cells == 0 else wholerange / sum_cells
        current_sum = 0
        result = []
        for cell in zip(cells, range(length)):
            size = int(int(cell[0]) * unit)
            result.append(size)
            current_sum += size
        result.append(wholerange - current_sum)
        return result

    @staticmethod
    def composite(cells, positions, canvas):
        result = [canvas.set_position((0, 0))]
        for cell, pos in zip(cells, positions):
            if cell is not None and cell.h > 0 and cell.w > 0:
                result.append(cell.set_position(pos))
        return CompositeVideoClip(result)

    @staticmethod
    def concatenate(clips):
        clips_f = map(lambda x: speedx(x, final_duration=round(x.duration, 9)), clips)
        clips_f = list(filter(lambda x: x.duration > 0, clips_f))
        checker = list(filter(lambda x: x.duration < 1 / x.fps, clips_f))
        if len(checker) > 0:
            raise ValueError("duration smaller than fps: " +
                             str(list(map(lambda x: x.duration, clips_f))))
        result = concatenate_videoclips(clips_f)
        return result

    @classmethod
    def cut_into_pieces(cls, inputclip, parts):
        dur_unit = inputclip.duration / sum(parts)
        durations = [i * dur_unit for i in parts]
        minimal = 1 / inputclip.fps
        durations = cls.kill(durations, lambda x: x < minimal)
        result = []
        start_current = 0
        for dur in durations:
            if dur is None:
                result.append(dur)
            else:
                # dirty fix for floating point imprecision
                end_current = round(start_current + dur, 9)
                start_current = round(start_current, 9)
                #
                result.append(inputclip.subclip(start_current, end_current))
                start_current = end_current
        assert(len(result) == len(parts))
        return result

    @staticmethod
    def kill(values, condition):
        maxi = max(values)
        idx = values.index(maxi)
        pre = values[:idx]
        post = values[(idx + 1):]
        dead_pre = filter(lambda x: condition(x), pre)
        dead_post = filter(lambda x: condition(x), post)
        sumkills = sum(dead_pre) + sum(dead_post)
        result_pre = [None if condition(x) else x for x in pre]
        result_post = [None if condition(x) else x for x in post]
        result = result_pre + [maxi + sumkills] + result_post
        assert(len(result) == len(values))
        return result

    @staticmethod
    def zoom(clip, factor):
        if clip.w > 0 and clip.h > 0:
            c = (1 - (1.0 / factor)) / 2
            x_crop = int(clip.w * c)
            y_crop = int(clip.h * c)
            width_crop = int(clip.w / factor)
            height_crop = int(clip.h / factor)
            cropped = crop(clip, x1=x_crop, y1=y_crop,
                           width=width_crop, height=height_crop)
            return cropped.resize(clip.size)
        else:
            return clip

    @classmethod
    def render_func(cls, vids, start_second, videofilename, audiofilename, preset):
        final_clip = cls.concatenate(vids)
        if audiofilename is not None:
            audio_clip = AudioFileClip(audiofilename, nbytes=4, fps=96000)
            audio_clip = audio_clip.subclip(
                start_second, start_second + final_clip.duration)
            final_clip = final_clip.set_audio(audio_clip)
        name = cls.make_name(videofilename)
        final_clip.write_videofile(name,
                                   codec='png',
                                   audio_fps=96000, audio_codec='pcm_s32le',
                                   audio_nbytes=4, preset=preset)
        return name

    def render(self, start_second, videofilename, audiofilename, preset):
        self.render_func(self.clips, start_second,
                         videofilename, audiofilename, preset)
        self.remove_tmps()

    def render_tmp(self, vid, tag):
        mkdir(self.build_dir + tag)
        name = tag + str(uuid.uuid4())
        fullname = self.render_func([vid], 0, name, None, 'ultrafast')
        self.tmps.append(fullname)
        result = VideoFileClip(fullname)
        return result

    def remove_tmps(self):
        for x in self.tmps:
            os.remove(os.getcwd() + "/" + x)

    @classmethod
    def make_name(cls, name):
        result = (cls.build_dir + name + ".avi")
        return result

    def black(self, dur):
        ref = self.clips[0]
        size = ref.size
        fps = ref.fps
        outputclip = ColorClip(size, (0, 0, 0), duration=dur).set_fps(fps)
        self.clips.append(outputclip)

    @staticmethod
    def check_dur(invid, outvid):
        assert(invid.fps == outvid.fps)
        inlen = round(invid.duration, 6)
        outlen = round(outvid.duration, 6)
        diff = round(abs(inlen - outlen), 6)
        minimum = round(1 / invid.fps, 6)
        if diff <= minimum:
            outputclip = speedx(outvid, final_duration=inlen)
        else:
            assert(inlen == outlen)
            outputclip == outvid
        return outputclip
