from threading import Thread

from config import sec
from sinesudoku.sudokusolver import Sudokusolver
from sinesudoku.sudokusynth import Sudokusynth
from sudokuscore_audio.section1 import score1
from sudokuscore_audio.section2 import score2
from sudokuscore_audio.section3 import score3
from sudokuscore_audio.section4 import score4
from sudokuscore_audio.section5 import score5
from sudokuscore_audio.section6 import score6
from sudokuscore_audio.section7 import score7
from sudokuscore_audio.section8 import score8
from sudokuscore_audio.section9 import score9


def compose(name, seclist, sr=96000, ksmps=1):
    # setup
    synth = Sudokusynth(name)
    time = 0

    # sections (volumes are old comments are old)
    # YES -19 distorts, -20 not
    time, sudoku = score1(synth, time, -24, seclist[0], 20)  # 15 minutes
    solver = Sudokusolver(sudoku, 573)
    # YES -33 distorts, -34 not
    time = score2(synth, time, -30, seclist[1], solver)  # SLOW 5 hours
    # YES -33 distorts, -33.7 not
    time, sec4cue = score3(synth, time, -30, seclist[2], solver)  # 1 hour
    # YES -14 distorts, -15 not
    time = score4(synth, time, -18, seclist[3], solver, sec4cue)  # SLOW 8hours
    # YES -11 distorts, -12 not
    time = score5(synth, time, -12, seclist[4], solver)  # 5 minutes
    # YES -5 distorts, -5.2 not
    time = score6(synth, time, -6, seclist[5], solver, 33)  # 1 minute
    # YES -13 distorts, -14 not
    time = score7(synth, time, -18, seclist[6], solver)  # 1 minute
    # YES -19 distorts, -20 not
    time = score8(synth, time, -18, seclist[7], solver)  # 1 minute
    # YES -33.2 distorts, -33.3 as well but its ok
    time = score9(synth, time, -36, seclist[8], solver)  # 30 minutes

    # finish
    synth.silence(time, 9)
    if sr > 0:
        synth.render(sr, ksmps)


def the_piece():
    t2 = Thread(target=sec2)
    t3 = Thread(target=sec3)
    t4 = Thread(target=sec4)
    t2.start()
    t3.start()
    t4.start()

    for s in [1, 5, 6, 7, 8, 9]:
        name = sec + str(s)
        seclist = [0] * 9
        seclist[s - 1] = 1
        compose(name, seclist)

    t2.join()
    t3.join()
    t4.join()


def sec2():
    compose("sec2", [0, 1, 0, 0, 0, 0, 0, 0, 0])


def sec3():
    compose("sec3", [0, 0, 1, 0, 0, 0, 0, 0, 0])


def sec4():
    compose("sec4", [0, 0, 0, 1, 0, 0, 0, 0, 0])
