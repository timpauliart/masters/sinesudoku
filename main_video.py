from glob import iglob
from os.path import basename, join, splitext

from moviepy.editor import VideoFileClip

from sinesudoku.sudokusolver import Sudokusolver
from sinesudoku.sudokuvideo import Sudokuvideosizer
from sudokuscore_video.section1 import score1
from sudokuscore_video.section2 import score2
from sudokuscore_video.section3 import score3
from sudokuscore_video.section4 import score4
from sudokuscore_video.section6 import score6
from sudokuscore_video.section7 import score7
from sudokuscore_video.section8 import score8
from sudokuscore_video.section9 import score9


def compose(name, seclist, audio, preset):

    # pool
    print("create videopool")
    dirs_root = ["faces", "partials", "hands", "wtfs"]
    dirs_sub = ["momente", "mikrophonie", "jeita"]
    videopool = {}
    for root in dirs_root:
        rootpool = {}
        for sub in dirs_sub:
            regex = join('material_hd', root, sub, '*.avi')
            names = list(iglob(regex, recursive=True))
            videos = [VideoFileClip(n) for n in names]
            subpool = dict(
                [(sub + "_" + splitext(basename(v.filename))[0], v) for v in videos])
            rootpool.update(subpool)
        videopool[root] = rootpool

    for directory in videopool.values():
        for v in directory.values():
            assert(v.size == [640, 480])
            assert(v.fps == 25)

    print("setup videosizer")
    videosizer = Sudokuvideosizer()

    beginner = [0, 56, 76.2, 108.2,
                162.2, None, 179.2,
                179.7, 180.2]
    idx = seclist.index(1)
    audio_start = beginner[idx]

    print("scoring")
    sudoku = score1(videosizer, videopool, seclist[0])
    solver = Sudokusolver(sudoku, 573)

    score2(videosizer, videopool, seclist[1], solver)
    score3(videosizer, videopool, seclist[2], solver)
    score4(videosizer, videopool, seclist[3], solver)
    # score5
    score6(videosizer, videopool, seclist[5], solver)
    score7(videosizer, videopool, seclist[6], solver)
    score8(videosizer, videopool, seclist[7], solver)
    score9(videosizer, videopool, seclist[8], solver)

    videosizer.render(audio_start, name, audio, preset)
